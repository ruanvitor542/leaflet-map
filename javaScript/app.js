
//Criando camadas base (basemaps)

var osm = L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png' , {});

var baserelief = L.tileLayer('https://tile.opentopomap.org/{z}/{x}/{y}.png', {});

var thetrail = L.geoJSON(trail, {
    color: '#800000',
    weight: 3,
    dashArray: '12 8 12',
});
thetrail.bindTooltip('Trilha de Brumadinho')

var map = L.map(document.getElementById('map'), {
    center: [-20.14096837425511, -44.206366539001465],
    zoom: 15,
    layers: [osm, thetrail]
});

var towns = L.geoJSON(trail_stops, 
    {pointToLayer: function(feature, latlng){
        return L.circleMarker(latlng, 
            {
                radius: 5, color: '#00008b',
                fillOpacity: 0.5,
            });
    },
      onEachFeature: function( feature, layer){
          var townName = feature.properties.feature_na;
          var elevation = feature.properties.elev_in_m;
          var lat = feature.properties.prim_lat_d;
          var lon = feature.properties.prim_lon_1;

          layer.bindPopup('Name: ' + townName +
                          'Elevation: ' + elevation +
                          'Lat/Lon: ' + lat + ' , ' + lon);
          layer.on('mouseover', function() {layer.openPopup();});
          layer.on('mouseout', function() {layer.closePopup();});
      }
    });

towns.addTo(map);

//Adicionando as camadas ao mapa
var baselayers = {
    'Shaded Relief': baserelief,
    'OpenStreetMap': osm
};

var overlays = {
    'Trilha': thetrail
};
L.control.layers(baselayers, overlays).addTo(map);

var scale = L.control.scale()
scale.addTo(map)

// Add attribution
map.attributionControl.addAttribution('National Map Topo');
map.attributionControl.addAttribution('OpenTopoMap');